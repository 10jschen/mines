#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui>
#include "minewidget.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    MineWidget *mineWidget;
    QLabel *labelStatus;

private slots:
    void on_action_Simple_triggered();
    void on_action_Middle_triggered();
    void on_action_Difficult_triggered();
    void on_action_About_triggered();
    void on_action_NewGame_triggered();
};

#endif // MAINWINDOW_H
