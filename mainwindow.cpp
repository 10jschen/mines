#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    mineWidget = new MineWidget;
    ui->centralWidget->layout()->addWidget(mineWidget);
    labelStatus = new QLabel;
    labelStatus->setFixedHeight(25);
    ui->centralWidget->layout()->addWidget(labelStatus);
    mineWidget->pixelPerBlock = 40;
    mineWidget->rowCount = mineWidget->columnCount = 9;
    mineWidget->totalMines = 10;
    mineWidget->labelStatus = labelStatus;
    mineWidget->updateSize();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_action_Simple_triggered()
{
    ui->action_Simple->setChecked(true);
    ui->action_Middle->setChecked(false);
    ui->action_Difficult->setChecked(false);
    mineWidget->columnCount = mineWidget->rowCount = 9;
    mineWidget->totalMines = 10;
    mineWidget->updateSize();
}

void MainWindow::on_action_Middle_triggered()
{
    ui->action_Simple->setChecked(false);
    ui->action_Middle->setChecked(true);
    ui->action_Difficult->setChecked(false);
    mineWidget->columnCount = mineWidget->rowCount = 12;
    mineWidget->totalMines = 20;
    mineWidget->updateSize();
}

void MainWindow::on_action_Difficult_triggered()
{
    ui->action_Simple->setChecked(false);
    ui->action_Middle->setChecked(false);
    ui->action_Difficult->setChecked(true);
    mineWidget->columnCount = mineWidget->rowCount = 15;
    mineWidget->totalMines = 30;
    mineWidget->updateSize();
}

void MainWindow::on_action_About_triggered()
{
    QMessageBox::information(this, "关于扫雷", "<b>作者：</b>愤怒的企鹅<br />"
                             "<b>联系方式：</b>raysoft@sina.cn");
}

void MainWindow::on_action_NewGame_triggered()
{
    mineWidget->updateSize();
}
