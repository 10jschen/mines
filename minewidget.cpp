#include "minewidget.h"
#include <QtGui>

MineWidget::MineWidget(QWidget *parent) :
    QWidget(parent)
{
}

void MineWidget::expand(int row, int col)
{
    if(!isLegalBlock(row, col) || mines[row][col] == true || dug[row][col] == true)
        return;
    dug[row][col] = true;
    if(marked[row][col])
    {
        marked[row][col] = false;
        --markedCount;
    }
    if(number[row][col] == 0)
    {
        expand(row - 1, col - 1);
        expand(row - 1, col);
        expand(row - 1, col + 1);
        expand(row, col - 1);
        expand(row, col + 1);
        expand(row + 1, col - 1);
        expand(row + 1, col);
        expand(row + 1, col + 1);
    }
}

bool MineWidget::isLegalBlock(int row, int col)
{
    if(row < 0 || col < 0)
        return false;
    if(row >= rowCount || col >= columnCount)
        return false;
    return true;
}

void MineWidget::updateSize()
{
    int n;
    this->setFixedSize(pixelPerBlock  * columnCount, pixelPerBlock * rowCount);
    finished = 0;
    markedCount = 0;
    labelStatus->setText(QString("总共有 %1 个雷，已经标记了 %2 个雷")
                         .arg(totalMines).arg(markedCount));
    for(int i = 0; i < rowCount; ++i)
        for(int j = 0; j < columnCount; ++j)
        {
            mines[i][j] = false;
            dug[i][j] = false;
            marked[i][j] = false;
            number[i][j] = 0;
        }

    n = this->totalMines;
    qsrand(QTime::currentTime().msec());
    while(n)
    {
        int i, j;
        i = qrand() % rowCount;
        j = qrand() % columnCount;
        if(mines[i][j] == false)
        {
            mines[i][j] = true;
            if(isLegalBlock(i - 1, j - 1)) ++number[i - 1][j - 1];
            if(isLegalBlock(i - 1, j)) ++number[i - 1][j];
            if(isLegalBlock(i - 1, j + 1)) ++number[i - 1][j + 1];
            if(isLegalBlock(i, j - 1)) ++number[i][j - 1];
            if(isLegalBlock(i, j + 1)) ++number[i][j + 1];
            if(isLegalBlock(i + 1, j - 1)) ++number[i + 1][j - 1];
            if(isLegalBlock(i + 1, j)) ++number[i + 1][j];
            if(isLegalBlock(i + 1, j + 1)) ++number[i + 1][j + 1];
            --n;
        }
    }
    this->update();
}

void MineWidget::mousePressEvent(QMouseEvent *e)
{
    if(finished)
        return;
    int i, j;
    i = e->y() / this->pixelPerBlock;
    j = e->x() / this->pixelPerBlock;
    if(!isLegalBlock(i, j))
        return;

    if(e->button() == Qt::RightButton && !dug[i][j])
    {
        marked[i][j] = !marked[i][j];
        if(marked[i][j]) ++markedCount;
        else --markedCount;
        labelStatus->setText(QString("总共有 %1 个雷，已经标记了 %2 个雷")
                             .arg(totalMines).arg(markedCount));
        this->update();
        return;
    }
    else if(e->button() != Qt::LeftButton || marked[i][j])
        return;

    if(mines[i][j] == true)
    {
        finished = 1; /* 挖到雷了，结束 */
        this->update();
        labelStatus->setText("你挂了！");
        QMessageBox::information(this, "提示", "你挂了！");
    }
    else
    {
        expand(i, j);
        /* 由于 expand() 函数可能会去掉一些标记，因此需要刷新一下提示信息 */
        labelStatus->setText(QString("总共有 %1 个雷，已经标记了 %2 个雷")
                             .arg(totalMines).arg(markedCount));
        int count;
        count = 0;
        for(i = 0; i < rowCount; ++i)
            for(j = 0; j < columnCount; ++j)
                if(!mines[i][j] && dug[i][j])
                    ++count;
        if(count + totalMines == rowCount * columnCount)
        {
            finished = 2;
            this->update();
            labelStatus->setText("你赢了！");
            QMessageBox::information(this, "提示", "你赢了！");
        }
        else
            this->update();
    }
}

void MineWidget::paintEvent(QPaintEvent *)
{
    QImage blockImage, bombImage, markImage;
    QPainter painter(this);
    painter.drawRect(0, 0, this->width() - 1, this->height() - 1);
    blockImage.load(":/block.png");
    blockImage = blockImage.scaled(pixelPerBlock, pixelPerBlock);
    bombImage.load(":/bomb.png");
    bombImage = bombImage.scaled(pixelPerBlock, pixelPerBlock);
    markImage.load(":/mark.png");
    markImage = markImage.scaled(pixelPerBlock, pixelPerBlock);
    painter.setBackground(QBrush(Qt::gray));
    QFont font;
    font.setBold(true);
    font.setPixelSize(pixelPerBlock);
    painter.setFont(font);
    for(int i = 0; i < rowCount; ++i)
        for(int j = 0; j < columnCount; ++j)
        {
            if(finished && mines[i][j])
                dug[i][j] = true;
            if(!dug[i][j])
            {
                if(marked[i][j])
                    painter.drawImage(pixelPerBlock * j, pixelPerBlock * i, markImage);
                else
                    painter.drawImage(pixelPerBlock * j, pixelPerBlock * i, blockImage);
            }
            else if(mines[i][j])
            {
                painter.drawImage(pixelPerBlock * j, pixelPerBlock * i, bombImage);
                painter.drawRect(pixelPerBlock * j, pixelPerBlock * i, pixelPerBlock, pixelPerBlock);
            }
            else if(number[i][j] == 0)
                painter.drawRect(pixelPerBlock * j, pixelPerBlock * i, pixelPerBlock, pixelPerBlock);
            else
            {
                painter.drawRect(pixelPerBlock * j, pixelPerBlock * i, pixelPerBlock, pixelPerBlock);
                switch(number[i][j])
                {
                case 1:
                    painter.setPen(QColor(Qt::blue));
                    break;
                case 2:
                    painter.setPen(QColor(Qt::green));
                    break;
                case 3:
                    painter.setPen(QColor(Qt::yellow));
                    break;
                default:
                    painter.setPen(QColor(Qt::red));
                    break;
                }
                painter.drawText(pixelPerBlock * j, pixelPerBlock * i, pixelPerBlock, pixelPerBlock,
                                 Qt::AlignCenter, QString("%1").arg(number[i][j]));
                painter.setPen(QColor(Qt::black));
            }
        }
}
