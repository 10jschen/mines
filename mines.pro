#-------------------------------------------------
#
# Project created by QtCreator 2012-05-14T00:30:06
#
#-------------------------------------------------

QT       += core gui

TARGET = mines
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    minewidget.cpp

HEADERS  += mainwindow.h \
    minewidget.h

FORMS    += mainwindow.ui

RESOURCES += \
    mines.qrc



