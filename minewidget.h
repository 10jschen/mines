#ifndef MINEWIDGET_H
#define MINEWIDGET_H

#include <QtGui>

class MineWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MineWidget(QWidget *parent = 0);
    void paintEvent(QPaintEvent *);
    void updateSize();
    void mousePressEvent(QMouseEvent *e);

public:
    int rowCount;
    int columnCount;
    int totalMines;
    int pixelPerBlock;
    QLabel *labelStatus;

private:
    bool mines[64][64]; /* 表示哪个地方有雷 */
    int number[64][64]; /* 表示周围有多少雷 */
    bool dug[64][64]; /* 是否已经挖开了 */
    bool marked[64][64]; /* 是否标记为地雷 */
    int finished; /* 等于 0 表示尚未完成， 等于 1 表示挖到雷了，等于 2 表示赢了 */
    int markedCount;

private:
    void expand(int row, int col);
    bool isLegalBlock(int row, int col);

public slots:

};

#endif // MINEWIDGET_H
